__authors__ = 'A. Achraf El Ghazi', 'Ulrich Frey'
__maintainer__ = 'A. Achraf El Ghazi', 'Ulrich Frey'
__email__ = 'aboubakr.elghazi@dlr.de'

from pathlib import Path
import uvicorn
from fastapi import FastAPI
from fastapi.responses import HTMLResponse
from src.forecastapi.forecast import forecast_implementation
from src.modpredictor.cli import load_conf
from src.modpredictor.data_model import PredictionRequest, PredictionResponse
from src.modpredictor.ml_func import load_model_and_metadata

def load_models(model_conf):
    models = {}
    for city, model_path in model_conf.items():
        models[city] = load_model_and_metadata(Path(model_path))
    return models


app = FastAPI()

# Load the saved models based on the YAML configuration
pred_conf_file = "models.yaml"
conf = load_conf(pred_conf_file)
pv_models = load_models(conf.get("pv_models", {}))
ev_models = load_models(conf.get("ev_models", {}))
hp_models = load_models(conf.get("hp_models", {}))



@app.get("/", response_class=HTMLResponse)
async def root():
    return """<html><body>Welcome to AMIRIS-API; Please check the <a href="/docs">documentation</a></body></html>"""


@app.post("/forecast_PV")
async def forecast_api_PV(request: PredictionRequest) -> PredictionResponse:
    return forecast_implementation(pv_models, request)

@app.post("/forecast_EV")
async def forecast_api_ev(request: PredictionRequest) -> PredictionResponse:
    return forecast_implementation(ev_models, request)

@app.post("/forecast_HP")
async def forecast_api_hp(request: PredictionRequest) -> PredictionResponse:
    return forecast_implementation(hp_models, request)


if __name__ == "__main__":
    uvicorn.run(app, host="127.0.0.1", port=8000)  # noqa
