__authors__ = 'A. Achraf El Ghazi'
__maintainer__ = 'A. Achraf El Ghazi'
__email__ = 'aboubakr.elghazi@dlr.de'

from src.modpredictor.data_model import PredictionRequest, PredictionResponse
from src.modpredictor.ml_func import scale, prepare_data


def forecast_implementation(models, request: PredictionRequest) -> PredictionResponse:
    """Returns given forecast"""

    # Determine the requested model or fallback to default
    model_id = request.modelId.lower() if request.modelId else 'default'
    model, model_metadata = models.get(model_id, models['default'])

    # Prepare prediction request for the pre-trained model
    pred_req_df = request.to_dataframe()
    data_scaled, _ = scale(pred_req_df, model_metadata.inputs, model_metadata.targets)
    X_scaled, y_actual_scaled = prepare_data(data_scaled, model_metadata.time, model_metadata.inputs.vars, model_metadata.targets.vars)

    # Compute prediction
    y_pred_scaled = model.predict(X_scaled)
    y_pred = model_metadata.targets.scaler.inverse_transform(y_pred_scaled)
    result = y_pred

    return PredictionResponse(netLoadPrediction=result)
