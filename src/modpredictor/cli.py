__version__ = '0.0.0'
__authors__ = 'A. Achraf El Ghazi'
__maintainer__ = 'A. Achraf El Ghazi'
__email__ = 'aboubakr.elghazi@dlr.de'

import glob
import sys
import yaml


def load_conf(conf_file_pattern):
    if len(sys.argv) > 1:
        conf_file = sys.argv[1]
    else:
        conf_files = glob.glob(conf_file_pattern)
        if not conf_files:
            print(f"No configuration files found matching the pattern '{conf_file_pattern}'")
            print(f"Usage: python <script> <config_file> (or put a unique {conf_file_pattern} in the working directory")
            sys.exit(1)
        elif len(conf_files) > 1:
            print(f"Multiple configuration files found matching the pattern '{conf_file_pattern}'. Please specify one.")
            print(f"Usage: python <script> <config_file> (or put a unique {conf_file_pattern} in the working directory")
            sys.exit(1)
        else:
            conf_file = conf_files[0]

    # Load configuration from YAML file
    with open(conf_file, "r") as f:
        config = yaml.safe_load(f)

    return config
