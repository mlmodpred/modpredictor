__version__ = '0.0.0'
__authors__ = 'A. Achraf El Ghazi'
__maintainer__ = 'A. Achraf El Ghazi'
__email__ = 'aboubakr.elghazi@dlr.de'

import sys
import pickle
from pathlib import Path, PosixPath
import pandas as pd
from keras import Sequential
from keras.src.callbacks import EarlyStopping, ModelCheckpoint
from keras.src.layers import InputLayer, LSTM, Dense
from keras.src.losses import MeanSquaredError
from keras.src.optimizers import Adam
from keras.src.saving.saving_api import load_model
from sklearn.preprocessing import StandardScaler, OneHotEncoder
import numpy as np

# Adjust sys.path to include the src directory
current_path = Path(__file__).resolve()
src_root = current_path.parents[1]
if str(src_root) not in sys.path:
    sys.path.append(str(src_root.parent))

from src.modpredictor.data_model import TrainedModel


UNSUPPORTED_DIRECTION = "{} is not supported as diction for the lag window."
UNSUPPORTED_LOOKUP_OFFSET = "Unsupported {} lookup window offset of {}. Backward/Forward lookup windows can only contain values prior/post the prediction start step."

class PathFixUnpickler(pickle.Unpickler):
    def find_class(self, module, name):
        if module == 'pathlib' and name == 'PosixPath':
            return Path
        return super().find_class(module, name)

def load_model_and_metadata(dir_path):
    # Get tech for later renaming of files
    if "hp_" in dir_path.name:
        tech = "_HP"
    elif "ev_" in dir_path.name:
        tech = "_EV"
    elif "pv_" in dir_path.name:
        tech = "_PV"
    else:
        raise ValueError(f"Unknown technology type for directory {dir_path}")

    metadata_path = dir_path / f"metadata{tech}.pkl"
    with open(metadata_path, 'rb') as f:
        metadata = PathFixUnpickler(f).load()

    if not isinstance(metadata, TrainedModel):
        raise TypeError(f"Expected TrainedModel object, but got {type(metadata)}")

    model = load_model(dir_path / metadata.model_path)
    return model, metadata

# def scale(data, cols, scaler=None):
def scale(data, *params):
    if isinstance(data, (str, Path)):
        df = pd.read_csv(data, sep=";", comment="#")
    elif isinstance(data, pd.DataFrame):
        df = data
    else:
        raise ValueError("Input must be either a file path (string or Path) to a CSV or a DataFrame object.")

    numerical_cols = df.select_dtypes(include=np.number).columns
    for param in params:
        numerical_features = [col for col in list(param.vars) if col in numerical_cols]
        non_numerical_features = [col for col in list(param.vars) if col not in numerical_cols]
        if not param.scaler:
            scaler = StandardScaler()
            scaler.fit(df[numerical_features])
            param.scaler = scaler

        # Transform numerical features
        df.loc[:, numerical_features] = param.scaler.transform(df[numerical_features])

        # if non_numerical_features:
        #     # Encode non-numerical columns using OneHotEncoder
        #     encoder = OneHotEncoder(drop='first')
        #     encoder.fit(df[non_numerical_features])  # Fitting OneHotEncoder on non-numerical columns
        #     df[non_numerical_features] = encoder.transform(df[non_numerical_features])

    return df, params

def fit_scaler(file_paths, cols, scaler=None):
    dfs = []
    for file_path in file_paths:
        dfs.append(pd.read_csv(file_path, sep=";", comment="#"))
    combined_df = pd.concat(dfs)
    if not scaler:
        scaler = StandardScaler()
    scaler.fit(combined_df[cols])

    return scaler

def split_binary(data, first_part=0.9):
    assert 0 < first_part <= 1

    first_part_rows = int(first_part * len(data))
    train_data = data.iloc[:first_part_rows]
    val_data = data.iloc[first_part_rows:]
    return train_data, val_data

def split_trenary(data, train_size=0.8, val_size=0.1):
    assert 0 < train_size + val_size <= 1

    data_size = len(data)
    train_data = data.iloc[:int(train_size * data_size)]
    val_data = data.iloc[int(train_size * data_size):int((train_size + val_size) * data_size)]
    test_data = data.iloc[int((train_size + val_size) * data_size):]
    return train_data, val_data, test_data

def pad(list, size, direction='start', value=0):
    if len(list) < size:
        padding = [value] * (size - len(list))
        if direction == 'start':
            list = padding + list
        elif direction == 'end':
            list = list + padding

    return list


def prepare_data(df, time_var, input_vars, target_vars):
    df = df[list(set(input_vars).union(set(target_vars)))]

    X, y = [], []
    input_windows = list(input_vars.values())
    target_windows = list(target_vars.values())
    max_look_back = max(window.get('Backward', 0) for window in input_windows + target_windows)
    max_look_forward = max(window.get('Forward', 0) for window in input_windows)

    for i in range(max_look_back, len(df) - max_look_forward + 1):
        # Create input values for each input variable according to its windows
        sequence_X, sequence_y = [], []
        for input_name, input_window in input_vars.items():
            look_back = input_window.get('Backward', 0)
            back_offset = input_window.get('BackwardOffset', -1)
            if back_offset >= 0:
                raise ValueError(UNSUPPORTED_LOOKUP_OFFSET.format("Backward"), back_offset)

            look_forward = input_window.get('Forward', 0)
            forward_offset = input_window.get('ForwardOffset', 0)
            if forward_offset < 0:
                raise ValueError(UNSUPPORTED_LOOKUP_OFFSET.format("Forward"), forward_offset)

            past_values = df[input_name].iloc[i + 1 + back_offset - look_back:i + 1 + back_offset]
            future_values = df[input_name].iloc[i + forward_offset:i + forward_offset + look_forward]

            all_values = []
            all_values.extend(pad(past_values.tolist(), max_look_back, "start"))
            all_values.extend(pad(future_values.tolist(), max_look_forward, "end"))

            sequence_X.append(all_values)

        # Create input values for each target variable according to its windows
        for target_name, target_window in target_vars.items():
            look_back = target_window.get('Backward', 0)
            back_offset = target_window.get('BackwardOffset', -1)
            if back_offset >= 0:
                raise ValueError(UNSUPPORTED_LOOKUP_OFFSET.format("Backward"), back_offset)

            look_forward = 0
            forward_offset = 0

            past_values = df[target_name].iloc[i + 1 + back_offset - look_back:i + 1 + back_offset]
            future_values = df[target_name].iloc[i + forward_offset:i + forward_offset + look_forward]

            all_values = []
            all_values.extend(pad(past_values.tolist(), max_look_back, "start"))
            all_values.extend(pad(future_values.tolist(), max_look_forward, "end"))

            sequence_X.append(all_values)

        X.append(sequence_X)

        # Target variable is a single sequence at the current time step
        target_name, target_window = list(target_vars.items())[0]
        look_back = 0
        look_forward = target_window.get('Forward', 0)
        sequence_y = df[target_name].iloc[i - look_back:i + look_forward + 1].tolist()
        y.append(sequence_y)

    return np.array(X), np.array(y)


# Function to train the LSTM model on a single CSV file
def create_and_train(model_dir, X_train, y_train, X_val, y_val, current_model=None, epochs=200, batch_size=32):

    # Build LSTM model -- size 4
    model = Sequential()
    model.add(InputLayer((X_train.shape[1], X_train.shape[2])))
    model.add(LSTM(units=64, return_sequences=True))
    model.add(LSTM(units=128, return_sequences=True))
    model.add(LSTM(units=256, return_sequences=True))
    model.add(LSTM(units=512, return_sequences=True))
    model.add(LSTM(units=256, dropout=0.10))
    model.add(Dense(units=64, activation='relu'))
    model.add(Dense(units=1, activation='linear'))

    # # Build LSTM model -- size 3
    # model = Sequential()
    # model.add(InputLayer((X_train.shape[1], X_train.shape[2])))
    # model.add(LSTM(64, return_sequences=True, dropout=0.2))
    # model.add(LSTM(128, return_sequences=True, dropout=0.2))
    # model.add(LSTM(128, dropout=0.2))
    # model.add(Dense(64, "relu"))
    # model.add(Dense(units=1, activation='linear'))

    # Build LSTM model -- size 2
    # model = Sequential()
    # model.add(InputLayer((X_train.shape[1], X_train.shape[2])))
    # model.add(LSTM(64, return_sequences=True))
    # model.add(LSTM(64, dropout=0.2))
    # model.add(Dense(16, activation='relu'))
    # model.add(Dense(y_train.shape[1], activation='linear'))

    # # Build LSTM model -- size 1
    # model = Sequential()
    # model.add(InputLayer((X_train.shape[1], X_train.shape[2])))
    # model.add(LSTM(64, return_sequences=True))
    # model.add(LSTM(64))
    # model.add(Dense(y_train.shape[1]))

    model.compile(loss=MeanSquaredError(), optimizer=Adam(learning_rate=0.001))
    # model.compile(loss=MeanSquaredError(), optimizer=Adam(learning_rate=0.0001))

    # If a current model is provided, set its weights as the initial state for training
    if current_model:
        model.set_weights(current_model.get_weights())

    # Implement Early Stopping
    early_stopping = EarlyStopping(monitor='val_loss',
                                   patience=50,
                                   restore_best_weights=True,
                                   min_delta=0.001)

    # Save the best model during training using ModelCheckpoint callback
    checkpoint_filepath = model_dir / "best_model_weights.keras"
    checkpoint_callback = ModelCheckpoint(filepath=checkpoint_filepath,
                                          monitor='val_loss',
                                          save_best_only=True,
                                          mode='min')

    # Train the model with Early Stopping
    history = model.fit(X_train, y_train,
                        validation_data=(X_val, y_val),
                        epochs=epochs,
                        batch_size=batch_size,
                        callbacks=[early_stopping, checkpoint_callback],
                        verbose=1)

    # Load the best model weights after training is complete
    model.load_weights(checkpoint_filepath)

    return model, history
