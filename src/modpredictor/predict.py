__version__ = '0.0.0'
__authors__ = 'A. Achraf El Ghazi'
__maintainer__ = 'A. Achraf El Ghazi'
__email__ = 'aboubakr.elghazi@dlr.de'

import sys
from pathlib import Path
import numpy as np
import pandas as pd
from matplotlib import pyplot as plt
from sklearn.metrics import mean_squared_error, mean_absolute_error

# Adjust sys.path to include the src directory
current_path = Path(__file__).resolve()
src_root = current_path.parents[1]
if str(src_root) not in sys.path:
    sys.path.append(str(src_root.parent))

from src.modpredictor.cli import load_conf
from src.modpredictor.ml_func import prepare_data, load_model_and_metadata, scale

# Load configuration
pred_conf_file = "pred_*.yaml"
conf = load_conf(pred_conf_file)
model_dir = Path(conf.get("model_dir", "output/model/"))
file_path_test = Path(conf.get("file_path_test", model_dir / "final_test_data.csv"))

# Load the saved model
best_model, model_metadata = load_model_and_metadata(model_dir)
best_model.summary()

# Reshape features for LSTM input
data_scaled, _ = scale(file_path_test, model_metadata.inputs, model_metadata.targets)

X_scaled, y_actual_scaled = prepare_data(data_scaled, model_metadata.time, model_metadata.inputs.vars,
                                         model_metadata.targets.vars)

# Predict on the scaled data
y_pred_scaled = best_model.predict(X_scaled)

# Rescale back y values to original range
y_pred = model_metadata.targets.scaler.inverse_transform(y_pred_scaled)
y_actual = model_metadata.targets.scaler.inverse_transform(y_actual_scaled)

# Identify and handle NaN values
nan_indices = np.isnan(y_actual) | np.isnan(y_pred)
y_actual = y_actual[~nan_indices]
y_pred = y_pred[~nan_indices]

# y_pred *= 39300000

# Calculate mean squared error
# mse_final = round(mean_squared_error(y_actual, y_pred), 3)
mse_final = mean_squared_error(y_actual, y_pred)
# rmse_final = round(np.sqrt(mse_final), 3)
rmse_final = np.sqrt(mse_final)
# mae_final = round(mean_absolute_error(y_actual, y_pred), 3)
mae_final = mean_absolute_error(y_actual, y_pred)
# min_val = round(np.min(y_actual),3)
min_val = np.min(y_actual)
# max_val = round(np.max(y_actual),3)
max_val = np.max(y_actual)
# mape_final = np.mean(np.abs((y_actual - y_pred) / y_actual)) * 100
smape_final = np.mean(2 * np.abs(y_actual - y_pred) / (np.abs(y_actual) + np.abs(y_pred))) * 100
max_mape_final = np.mean(np.abs(y_actual - y_pred) / np.maximum(np.abs(y_actual), np.abs(y_pred))) * 100
print(f'Final RMSE on a test set: {rmse_final}')
print(f'Final MAE on a test set: {mae_final}')
print(f'Final SMAPE on a test set: {smape_final}%')
print(f'Final MaxMAPE on a test set: {max_mape_final}%')

# Save error metrics in summary.csv
prediction_data = pd.DataFrame(data={"Predictions": y_pred.flatten(), "Actuals": y_actual.flatten()})
prediction_data.to_csv(model_dir / "prediction.csv", header=True, sep=";", index=False)
summary_data = pd.DataFrame(data={"MSE": mse_final, "RMSE": rmse_final, "MAE": mae_final, "MinVal": min_val,
                                  "MaxVal": max_val}, index=[0])
summary_data.to_csv(model_dir / "summary.csv", header=True, sep=";", index=False)

# Plot results
plt.figure(figsize=(10, 6))
plt.plot(y_actual, label='Actual')
plt.plot(y_pred, label='Predicted')
plt.title('Actual vs Predicted Values on Test Set')
plt.xlabel('TimeStep')
plt.ylabel(list(model_metadata.targets.vars)[0])
plt.legend()
plt.show()

# Load model history
history = model_metadata.model_history
# print(history)
plt.plot(history["loss"], label="Training Loss")
plt.plot(history["val_loss"], label="Validation Loss")
plt.legend()
plt.show()
