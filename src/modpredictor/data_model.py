__authors__ = 'A. Achraf El Ghazi'
__maintainer__ = 'A. Achraf El Ghazi'
__email__ = 'aboubakr.elghazi@dlr.de'

import pickle
from dataclasses import dataclass
from pathlib import Path
from typing import Any, Dict, Optional
import pandas as pd
from pydantic import BaseModel
from sklearn.base import TransformerMixin, BaseEstimator


@dataclass
class Parameter:
    """The type of possibally scaled input variable, including the target variable"""
    vars: Any = None
    scaler: Any = None


@dataclass
class TrainedModel:
    """The type of pre-trained model"""
    time: str = None
    inputs: Parameter = Parameter()
    targets: Parameter = Parameter()
    model_class: Any = None
    model_path: Path = None
    model_history: Any = None


    def save(self, model_dir_path, tech):
        """Stores in the given directory path the TrainedModel object"""

        metadata_file_path = model_dir_path / f"metadata{tech}.pkl"
        with open(metadata_file_path, 'wb') as f:
            pickle.dump(self, f)



class IdScaler(BaseEstimator, TransformerMixin):
    def __init__(self):
        pass

    def fit(self, X, y=None):
        return self

    def transform(self, X):
        return X

    def inverse_transform(self, X):
        return X

class InputVariable(BaseModel):
    name: str
    values: Dict[int, float]

class PredictionRequest(BaseModel):
    """Input for forecast API"""
    modelId: Optional[str] = None
    predictionStart: int
    inputVars: list[InputVariable]

    def to_dataframe(self) -> pd.DataFrame:
        """Transform the object's data into a DataFrame suitable for model input"""
        # Extract the field names and values
        data = {
            input_var.name: {
                time: value
                for time, value in input_var.values.items()
            }
            for input_var in self.inputVars
        }

        # Create the DataFrame
        df = pd.DataFrame(data)

        # Ensure predictionStart is included in the index without overwriting existing values
        if self.predictionStart not in df.index:
            df.loc[self.predictionStart] = pd.Series(dtype=float)

        # Fill missing values with 0 and sort wrt index
        df = df.fillna(0)
        df = df.sort_index()

        return df

class PredictionResponse(BaseModel):
    """Output of forecast API"""
    netLoadPrediction: float