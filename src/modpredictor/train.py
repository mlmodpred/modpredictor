__version__ = '0.0.0'
__authors__ = 'A. Achraf El Ghazi'
__maintainer__ = 'A. Achraf El Ghazi'
__email__ = 'aboubakr.elghazi@dlr.de'

import json
import sys
import datetime
from pathlib import Path
import pandas as pd
from sklearn.model_selection import KFold

# Adjust sys.path to include the src directory
current_path = Path(__file__).resolve()
src_root = current_path.parents[1]
if str(src_root) not in sys.path:
    sys.path.append(str(src_root.parent))

from src.modpredictor.cli import load_conf
from src.modpredictor.data_model import TrainedModel, Parameter, IdScaler
from src.modpredictor.ml_func import prepare_data, create_and_train, fit_scaler, scale, split_binary



# Load configuration
train_conf_file = "train_*.yaml"
conf = load_conf(train_conf_file)

# Extract configuration variables
file_paths_train = conf.get("file_paths_train", [])
test_ratio = conf.get("test_ratio", 0)
time_var = conf.get("time_var", "")
input_vars = conf.get("input_vars", {})
target_vars = conf.get("target_vars", {})
model_dir = Path(conf.get("model_dir", "output/model/"))
scaling = conf.get("scaling", [])
epochs = conf.get("epochs", 200)
cross_val_folds = conf.get("cross_val_folds", 5)

# Make new default directory if not exists or make a new one with date in name and set this as model_dir
if model_dir.exists():
    formatted_date = datetime.datetime.now().strftime("_%d_%m_%Y_%H_%M")
    model_dir.rename(model_dir.parent / (model_dir.name + formatted_date))
model_dir.mkdir(parents=True)


# Get tech for later renaming of files
if "hp_" in str(model_dir):
    tech = "_HP"
elif "ev_" in str(model_dir):
    tech = "_EV"
elif "pv_" in str(model_dir):
    tech = "_PV"
else:
    raise ValueError(f"Unknown technology type for directory {model_dir}")

# Initialize model and history
best_model = None
history = None

def process():
    global best_model, history
    final_test_data = pd.DataFrame()

    input_vars_list = list(input_vars)
    target_vars_list = list(target_vars)

    scaler_input_vars = fit_scaler(file_paths_train, input_vars_list) if "input" in scaling else IdScaler()
    scaler_target_vars = fit_scaler(file_paths_train, target_vars_list) if "target" in scaling else IdScaler()

    input_vars_param = Parameter(input_vars, scaler_input_vars)
    target_vars_param = Parameter(target_vars, scaler_target_vars)


    # Train the model incrementally
    best_val_loss = float('inf')
    final_history = None
    for file_path in file_paths_train:
        df = pd.read_csv(file_path, sep=";", comment="#")
        train_val_data, test_data = split_binary(df, (1 - test_ratio))

        # Append the test data to the final DataFrame
        if final_test_data.empty:
            final_test_data = test_data.copy()
        else:
            final_test_data = pd.concat([final_test_data, test_data], ignore_index=True)

        train_val_data, params = scale(train_val_data, input_vars_param, target_vars_param)

        kf = KFold(n_splits=cross_val_folds)
        for train_index, val_index in kf.split(train_val_data):
            train_data = train_val_data.iloc[train_index]
            val_data = train_val_data.iloc[val_index]

            # train_data, val_data = split_binary(train_data, 0.9)

            # Process the data for training
            X_train, y_train = prepare_data(train_data, time_var, input_vars, target_vars)
            X_val, y_val = prepare_data(val_data, time_var, input_vars, target_vars)

            # Train the model
            model, history = create_and_train(model_dir, X_train, y_train, X_val, y_val, current_model=best_model, epochs=epochs, batch_size=32)

            # Get the best validation loss from the history (this should already reflect the best epoch)
            best_current_val_loss = min(history.history['val_loss'])

            # Check if this model has the best validation loss across all files/folds
            if best_current_val_loss < best_val_loss:
                best_val_loss = best_current_val_loss
                best_model = model

            # Append the current history to the final history
            if final_history is None:
                final_history = {key: [] for key in history.history.keys()}
            for key in final_history.keys():
                if key in history.history:
                    final_history[key].extend(history.history.get(key, []))

    # Save the model to file
    trained_model_file_name = Path(f"model{tech}.keras")
    trained_model_path = model_dir / trained_model_file_name
    best_model.save(trained_model_path, tech)
    trained_model_obj = TrainedModel(time_var, input_vars_param, target_vars_param, best_model.__class__, trained_model_file_name, final_history)
    trained_model_obj.save(model_dir, tech)

    # Save the final DataFrame containing all test data to a CSV file
    final_test_data.to_csv(model_dir / ("final_test_data" + tech + ".csv"), header=True, sep=";", index=False)

    # Save model details
    model_details = pd.DataFrame([{
        "File paths": " | ".join([str(file_path) for file_path in file_paths_train]),
        "Number of files": len(file_paths_train),
        "Number of folds per file": cross_val_folds,
        "Total epochs trained": len(final_history['val_loss']),
        "Best validation loss": best_val_loss,
        "Best training loss": min(final_history['loss'])
    }])
    pd.DataFrame.to_csv(model_details, path_or_buf=model_dir / ("model_details" + tech + ".csv"), header=True, sep=";", index=False)


if __name__ == "__main__":
    process()
