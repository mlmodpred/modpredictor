# MLModPed, a Multi-Input Time Series Forecasting Framework

## 📖 Project Description
This project implements a deep learning solution for forecasting time series with multiple input variables using an LSTM-based neural network. 
It is designed to preserve the sequential nature of time series data and flexibly handle multiple input variables. 
The training process is highly configurable and supports incremental training on multiple CSV files with cross-validation, custom backward/forward windowing for both input and target variables, and model metadata preservation for later predictions.
Exemplary input data is in the /input/ folder. There is data for heat pumps (HP), electric vehicles (EV), and Photovoltaic+Storage systems (PV).

## 🚀 Key Features

- **Data Sequentiality Preservation:**  
  The project maintains the sequential integrity of time series data during both preprocessing and training. Custom backward and forward windowing ensures that each training sequence correctly reflects the temporal context.

- **Multi-Input Variable Handling:**  
  Support for multiple input variables, each with its own configurable backward (past) and forward (future) window parameters, allowing the model to capture complex temporal dependencies.

- **Custom Windowing per Variable:**  
  Each input and target variable can be provided with individual window parameters:
  - **Backward Window:** Number of past time steps to consider.
  - **Forward Window:** Number of future time steps to include (for inputs) or predict (for targets).

- **Cross-Validation:**  
  Uses K-Fold cross-validation to robustly evaluate model performance and select the best model based on validation loss.

- **Incremental Training on Multiple Files:**  
  Supports incremental training by sequentially processing multiple CSV files. This is especially useful when dealing with large datasets or integrating data from various sources.

- **Model and Metadata Persistence:**  
  After training, the best model is saved along with its metadata (scaling parameters, variable configurations, training history, etc.). This facilitates easy reloading of the model for future predictions.

- **Data Scaling:**  
  Implements data scaling for both input and target variables using a `StandardScaler`, with an option to use an identity scaler (`IdScaler`) when scaling is not desired.

- **Flexible Configuration:**  
  All key parameters (file paths, variable configurations, training epochs, cross-validation folds, etc.) are managed through YAML configuration files. This enables easy adjustments without modifying the core code.

- **Prediction and Evaluation:**  
  The prediction module loads the saved model and metadata, scales test data accordingly, computes predictions, and evaluates performance using metrics like MSE, RMSE, MAE, SMAPE, and more. It also generates plots for visual comparison and error analysis.


## Usage

### Training the Model

To train a model, use the `train.py` script along with the appropriate YAML configuration file (e.g., `train_hp.yaml`, `train_ev.yaml`, or `train_pv.yaml`). The configuration file specifies the input CSV file paths, variable window parameters, training settings (such as epochs and cross-validation folds), and output directories. For example, to train a photovoltaic (PV) model, run:

```bash
python train.py
```

During training, the script will:

- Incrementally load and process multiple CSV files.
- Split each dataset into training/validation and test subsets.
- Apply data scaling based on the provided configuration.
- Prepare input and target sequences using custom backward and forward window settings.
- Perform K-Fold cross-validation and select the best model based on validation loss.
- Save the best model along with its metadata (scaling parameters, variable configurations, training history, etc.) in the specified output directory.
- Export the aggregated test data and model details as CSV files.

### Making Predictions
To perform predictions using a trained model, use the predict.py script along with the corresponding prediction YAML configuration (e.g., pred_hp.yaml, pred_ev.yaml, or pred_pv.yaml). The prediction configuration specifies the model directory and input test data file (defaulting to the final_test_data.csv produced during training, unless you provide your own time series). 
To predict, run:

```bash
python predictj.py
```

The prediction script will:

- Load the saved model and its metadata.
- Scale and prepare the test data in the same manner as during training.
- Compute predictions and evaluate performance using metrics like MSE, RMSE, MAE, and SMAPE.
- Generate plots comparing actual vs. predicted values, training history, and error distributions.
- Save the prediction results, summary metrics, and plots in the designated output directory.

## Contributing
Feel free to contribute via new issues or merge requests.

## Authors and acknowledgment
The authors of this project are Aboubakr Achraf El Ghazi, Ulrich Frey, Fabia Miorelli, and Evelyn Sperber.

## Contact
For questions, support and feedback, please contact: A. A. El Ghazi (aboubakr.elghazi@dlr.de).

## License
The licence is Apache 2.0
